public interface Logic {
    void increaseTime();
    void decreaseTime();
    void moveLeft();
    void moveRight();
    int getDegrees();
    double getDistance();
}
