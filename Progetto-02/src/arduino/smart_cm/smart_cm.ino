#include "include/servo_motor_impl.h"
#include "include/Task.h"
#include "include/MotorSonarTask.h"
#include "include/Scheduler.h"
#include "include/PirDetectionTask.h"
 
#define minDegree 0
#define maxDegree 170;
#define pinPir 9
#define pinMotor 2
#define pinSonar 5
#define trigPin 4
#define echoPin 7


int pos;  
int s = 10;
int currentPos = 0;
bool single=true, manual = false, automatic = false;
ServoMotor* pMotor;
Task* mst;
Task* pdt;
Scheduler sched;

void setup() {
  Serial.begin(9600);
  
  mst = new MotorSonarTask(pinMotor, pinSonar, trigPin, echoPin);
  pdt = new PirDetectionTask(pinPir, mst);
  sched.init(100);
  mst->init(100);
  pdt->init(100);
  
  /*Task* t0 = new BlinkTask(13);
  t0->init(100);

  Task* t1 = new PrintTask();
  t1->init(1000);
  
  sched.addTask(t0);
  sched.addTask(t1);*/
}

void loop() {

   while(single){
      pdt->tick();
   }
   while(manual){
    
   }
   while(automatic){
    
   }
}
