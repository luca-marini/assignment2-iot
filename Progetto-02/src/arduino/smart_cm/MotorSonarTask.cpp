#include "MotorSonarTask.h"
#include "Arduino.h"

extern int s;
extern int currentPos;
extern int maxDegree;
extern int minDegree;
extern bool towardsZero;

MotorSonarTask::MotorSonarTask(int pinMotor){
  this->pinMotor = pinMotor;
}
  
void MotorSonarTask::init(int period){
  Task::init(period);
  pMotor = new ServoMotorImpl(pinMotor);
  pMotor->on();
}
  
void MotorSonarTask::tick(){
  if(towardsZero){
    currentPos = currentPos - s;
  }else{
    currentPos = currentPos + s;
  }
  this->adjustPos();
  pMotor->setPosition(currentPos);
}

void adjustPos(){
  if(currentPos > maxDegree){
     currentPos = maxDegree;
     towardsZero = true;
  }else if(currentPos < minDegree){
     currentPos = minDegree; 
     towardsZero = false;
  }
  
}
