#include "include/PirImpl.h"
#include "Arduino.h"

PirImpl::PirImpl(int pin){
  this->pin = pin;
  pinMode(pin, INPUT);     
} 
  
bool PirImpl::detect(){
  Serial.println(digitalRead(pin));
  return digitalRead(pin) == HIGH;
}
