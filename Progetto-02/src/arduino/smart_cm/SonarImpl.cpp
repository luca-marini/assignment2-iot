#include "include/SonarImpl.h"
#include "Arduino.h"

SonarImpl::SonarImpl(int trigPin, int echoPin){
  this->trigPin = trigPin;
  this->echoPin = echoPin;
  pinMode(trigPin, OUTPUT);     
  pinMode(echoPin, INPUT);     
} 
  
float SonarImpl::getDistance(){
  /* invio impulso */
    const float vs = 331.45 + 0.62*20;

    digitalWrite(trigPin,LOW);
    delayMicroseconds(3);
    digitalWrite(trigPin,HIGH);
    delayMicroseconds(5);
    digitalWrite(trigPin,LOW);
    
    /* ricevi l’eco */
    float tUS = pulseIn(echoPin, HIGH);
    float cm = (tUS/2) / 29.1;
    //float t = tUS / 1000.0 / 1000.0 / 2;
    //float d = t*vs;
    return cm;
}

float SonarImpl::getMaximumDistance(){
    return this->maximumDistance;
}