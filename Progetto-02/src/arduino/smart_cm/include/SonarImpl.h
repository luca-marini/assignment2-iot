#ifndef __SONARIMPL__
#define __SONARIMPL__

#include "Sonar.h"

class SonarImpl: public Sonar {
 
public: 
  SonarImpl(int pin1, int pin2);
  float getDistance();
  float getMaximumDistance();

private:
  int trigPin;
  int echoPin;
  const float maximumDistance = 400;

};

#endif
