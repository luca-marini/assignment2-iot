#ifndef __PIRDETECTIONTASK__
#define __PIRDETECTIONTASK__

#include "Task.h"
#include "Pir.h"
#include "PirImpl.h"
#include "MotorSonarTask.h"


class PirDetectionTask: public Task {
  
public:

  PirDetectionTask(int pin,Task* mst);  
  void init(int period);  
  void tick();
  
private:
  Pir* pir;
  Task* mst;
  int pin;
  int maxDegree = 170;
  int minDegree = 0;
};


#endif
