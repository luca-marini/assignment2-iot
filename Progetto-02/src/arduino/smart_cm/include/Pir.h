#ifndef __PIR__
#define __PIR__

class Pir {
 
public: 
  virtual bool detect() = 0;
};

#endif
