#ifndef __DETECTPOSITIONTASK__
#define __DETECTPOSITIONTASK__

#include "Task.h"
#include "Sonar.h"
#include "SonarImpl.h"
#include "MsgService.h"

class DetectPositionTask: public Task {


public:

  DetectPositionTask(int trigPin, int echoPin);  
  void init(int period);  
  void tick();

private:
  Sonar* sonar;
  int pinSonar;
  int trigPin;
  int echoPin;
};

#endif
