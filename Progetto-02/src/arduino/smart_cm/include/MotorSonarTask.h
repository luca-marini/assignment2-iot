#ifndef __MOTORSONARTASK__
#define __MOTORSONARTASK__

#include "Task.h"
#include "servo_motor.h"
#include "servo_motor_impl.h"

class MotorSonarTask: public Task {


public:

  MotorSonarTask(int pinMotor);  
  void init(int period);  
  void tick();

private:
  ServoMotor* pMotor;
  int pinMotor;
};

#endif
