#include "include/DetectPositionTask.h"
#include "Arduino.h"

extern int currentPos;

DetectPositionTask::DetectPositionTask(int trigPin, int echoPin){
	this->pinSonar = pinSonar;
	this->trigPin = trigPin;
	this->echoPin = echoPin;
}
  
void DetectPositionTask::init(int period){
    Task::init(period);
    this->sonar = new SonarImpl(trigPin, echoPin);
    MsgService.init();
}
  
void DetectPositionTask::tick(){
 	float distance = sonar->getDistance();
 	if(distance < sonar->getMaximumDistance()){
  		//setactive led Ld task
  		MsgService.sendMsg("detected"); 
  	  	MsgService.sendMsg(String(distance)); // serve riferimento al msgService
  		MsgService.sendMsg(String(currentPos));
  	}


  


}
