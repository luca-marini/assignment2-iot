#include "include/PirDetectionTask.h"
#include "Arduino.h"

extern int s;
extern int currentPos;
PirDetectionTask::PirDetectionTask(int pin, Task* mst){
  this->pin = pin;
  this->mst = mst;
}
  
void PirDetectionTask::init(int period){
  Task::init(period);
  pir = new PirImpl(pin);
}
  
void PirDetectionTask::tick(){
  if(pir->detect()){
    for(int i = this->minDegree; i < this->maxDegree; i += s){
      currentPos = i;
      mst->tick();
      delay(100);
    }
  }
}
